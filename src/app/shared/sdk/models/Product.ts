/* tslint:disable */

export interface ProductInterface {
  name: string;
  type: string;
  rate: number;
  duration: number;
  rateType: string;
  id?: any;
}

export class Product implements ProductInterface {
  name: string;
  type: string;
  rate: number;
  duration: number;
  rateType: string;
  id: any;
  constructor(instance?: Product) {
    Object.assign(this, instance);
  }
}
