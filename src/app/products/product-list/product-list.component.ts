import { Component, OnInit } from '@angular/core';
import {ProductApi} from "../../shared/sdk/services/custom/Product";
import {Comparator} from "clarity-angular";
import {Product} from "../../shared/sdk/models/Product";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  data: any[];
  datagridLoaded: boolean = false;
  private productComparator = new ProductComparator();

  constructor(private productService: ProductApi) { }

  ngOnInit() {
    this.refreshList();
  }

  refreshList() {
    this.productService.find().subscribe((resp) => {
      this.data = resp;
      this.datagridLoaded = true;
    })
  }
}

class ProductComparator implements Comparator<Product> {
  compare(a: Product, b: Product) {
    return a.rate - b.rate;
  }
}
