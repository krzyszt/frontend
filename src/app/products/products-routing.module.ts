import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent} from "./product/product.component";
import { TopProductsComponent } from "./top-products/top-products.component";
import { ProductListComponent } from "./product-list/product-list.component";

const productRoutes: Routes = [
  {
    path: '',
    component: ProductComponent,
    children: [
      {
        path: '',
        component: TopProductsComponent
      },
      {
        path: 'top-products',
        component: TopProductsComponent
      },
      {
        path: 'products-list',
        component: ProductListComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(productRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ProductRoutingModule {}
