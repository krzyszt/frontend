import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule} from "clarity-angular/index";
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ProductComponent } from './product/product.component';
import { ProductListComponent} from "./product-list/product-list.component";
import { ProductRoutingModule} from "./products-routing.module";
import { TopProductsComponent } from './top-products/top-products.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    ClarityModule,
    ProductRoutingModule
  ],
  declarations: [
    ProductComponent,
    ProductListComponent,
    TopProductsComponent
  ]
})
export class ProductModule { }
