import { Component, OnInit } from '@angular/core';
import {ProductApi} from "../../shared/sdk/services/custom/Product";
import * as _ from "lodash";

@Component({
  selector: 'app-top-products',
  templateUrl: './top-products.component.html',
  styleUrls: ['./top-products.component.css']
})
export class TopProductsComponent implements OnInit {
  data: any[];
  datagridLoaded: boolean = false;

  constructor(private productService: ProductApi) { }

  ngOnInit() {
    this.refreshList();
  }

  refreshList() {
    this.productService.find().subscribe((resp) => {
      this.data = _.slice(_.orderBy(resp, ['rate'], ['desc']), 0, 3);
      this.datagridLoaded = true;
    })
  }

}
