import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserApi} from "../shared/sdk/services/custom/User";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  currentUser = {
    "username": "admin",
    "password": "admin"
  }
  isAuthenticated: boolean = true;

  constructor(private userService: UserApi, private router: Router) { }

  ngOnInit() {}

  login() {
    this.userService.login(this.currentUser).subscribe((resp) => {
      this.isAuthenticated = true;
      this.router.navigate(['/products']);
    });
  }

}
